var vm = new Vue({
    el: "#cart-main",
    data: {
        productList: [],
        checkAllFlag: false,
        totalPrice: 0,
        delFlag: false,
        curProduct: ''
    },
    mounted: function() {
        this.$nextTick(function() {
            this.getProdcutList();
        });
    },
    filters: {
        priceFormatFilter: function(value) {
            return "¥" + value.toFixed(2) + "元";
        }
    },
    methods: {
        getProdcutList: function() {
            var _this = this;
            this.$http.get("data/cart.json").then(
                function(response) {
                    console.log(response);
                    _this.productList = response.body;
                },
                function(response) {
                    alert("请求错误");
                }
            );
        },
        changeMoney: function(item, type) {
            if (type > 0) {
                item.productNumber++;
            } else {
                item.productNumber--;
                if (item.productNumber < 1) {
                    item.productNumber = 1;
                }
            }
            this.calcTotalPrice();
        },
        selectProduct: function(item) {
            if (typeof item.checked == "undefined") {
                this.$set(item, "checked", true);
            } else {
                item.checked = !item.checked;
            }
            this.calcTotalPrice();
        },
        checkAll: function(flag) {
            this.checkAllFlag = flag;
            var _this = this;
            this.productList.forEach(function(item, index) {
                if (typeof item.checked == 'undefined') {
                    _this.$set(item, "checked", _this.checkAllFlag);
                } else {
                    item.checked = _this.checkAllFlag;
                }
            });
            this.calcTotalPrice();
        },
        calcTotalPrice: function() {
            var _this = this;
            this.totalPrice = 0;
            this.productList.forEach(function(item, index) {
                if (item.checked) {
                    _this.totalPrice += item.productNumber * item.productPrice;
                }
            })
        },
        delConfirm: function(item) {
            this.delFlag = true;
            this.curProduct = item;
        },
        delProduct: function() {
            var index = this.productList.indexOf(this.curProduct);
            this.productList.splice(index, 1);
            this.delFlag = false;
        }
    }
});

/**
 * 给数字加上逗号如：7,145,214.00{
 * }
 */
function comma(num, length) {
    if (num.constructor != Number)
        return 0;
    if (!length || length < 1) {
        length = 3;
    }
    num = String(num).split(".");
    num[0] = num[0].replace(new RegExp('(\\d)(?=(\\d{' + length + '})+$)', 'ig'), "$1,");
    return num.join(".");
}